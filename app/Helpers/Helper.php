<?php

namespace App\Helpers;
use Carbon\Carbon;

class Helper
{
	public static function setActive($path, $active = 'active')
	{
		return call_user_func_array('Request::is', (array)$path) ? $active : '';
	}
	
	public static function getUserAge($personalCode)
	{
		$centuaryDigit	= substr($personalCode, 0, 1);
		$centuaryArray	= self::getCentuaryArray();
		$centuary		= $centuaryArray[$centuaryDigit];
		$yearLastDigit	= substr($personalCode, 1,2);
		$month			= substr($personalCode, 3,2);
		$date			= substr($personalCode, 5,2);
		if($yearLastDigit == '00')
			$yearStartDigit	=	$centuary;
		else
			$yearStartDigit	=	$centuary - 1;
		$birthYear		=	$yearStartDigit.$yearLastDigit;
		$dateOfBirth	=	$birthYear.'-'.$month.'-'.$date;

		$age 			= 	Carbon::parse($dateOfBirth)->age;
		return $age;
	}
	
	public static function getCentuaryArray()
	{
		return $centuaryArray = [
			'1'	=> 	19,
			'2'	=> 	19,
			'3' =>	20,
			'4' =>	20,
			'5' =>	21,
			'6' =>	21
		];
	}
	
}

