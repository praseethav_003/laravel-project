<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Loan;

class LoanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('loans');
    }
	
	public function getLoanList()
	{
		$loanList	=	Loan::latest()->get();
		$loanArray	= 	array();
		if(!empty($loanList)){
			foreach($loanList as $key=>$val){
				$loanArray[$key]['id']			= $val->id;
				$loanArray[$key]['user_id']		= $val->user_id;
				$loanArray[$key]['amount']		= $val->amount;
				$loanArray[$key]['interest']	= $val->interest;
				$loanArray[$key]['duration']	= $val->duration;
				$loanArray[$key]['start_date']	= $val->start_date;
				$loanArray[$key]['end_date']	= $val->end_date;
				$loanArray[$key]['campaign']	= $val->campaign;
				$loanArray[$key]['status']		= $val->status;
				$loanArray[$key]['user_name'] 	= $val->user->first_name.' '.$val->user->last_name;
			}
		}
		$userList	=	User::select('first_name','last_name','user_id')->where('role','user')->orderBy('first_name')->get();
		$returnArray['loans']	=	$loanArray;
		$returnArray['users']	=	$userList;
		return response()->json($returnArray);
	}
	
	public function saveLoan(Request $request)
	{
		$returnArray = array();
		$this->validate($request, [
            'amount' => 'required|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'interest' => 'required|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'duration' => 'required|gt:0|numeric',
            'start_date' => 'required|date|before:end_date',
            'end_date' => 'required|date|after:start_date',
            'campaign' => 'required|numeric',
            'status' => 'required|integer',
            'user' => 'required'
        ]);
		
		if($request->loan_id){
			$loan	=	Loan::find($request->loan_id);
			if(!empty($loan)){
				$loan->user_id		=	$request->user;
				$loan->amount		=	$request->amount;
				$loan->interest		=	$request->interest;
				$loan->duration		=	$request->duration;
				$loan->start_date	=	$request->start_date;
				$loan->end_date		=	$request->end_date;
				$loan->campaign		=	$request->campaign;
				$loan->status		=	$request->status;
				$loan->save();
			}
			$returnArray['message']		=	"Loan details has been saved";
		}else{
			Loan::create([
				'user_id' 		=> $request->user,
				'amount' 		=> $request->amount,
				'interest' 		=> $request->interest,
				'duration' 		=> $request->duration,
				'start_date' 	=> $request->start_date,
				'end_date' 		=> $request->end_date,
				'campaign' 		=> $request->campaign,
				'status' 		=> $request->status
			  ]);
			  $returnArray['message']	=	"Loan has been created successfully";
		}
        return response()->json($returnArray, 200);
	}
	
	public function deleteLoan(Request $request)
	{
		$loan = Loan::find($request->loan_id);
		$loan->status = 3;
		$loan->save();
		$returnArray['message']	=	"Loan has been deleted successfully";
		return response()->json($returnArray);
	}
}
