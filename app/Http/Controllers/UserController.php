<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Helper;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('users');
    }
	
	public function getUsersList()
	{
		$userList	=	User::where('role','user')->latest()->get();
		$userArray	= 	array();
		if(!empty($userList)){
			foreach($userList as $key=>$val){
				$userArray[$key] = $val;
				$userArray[$key]['age'] = Helper::getUserAge($val->personal_code);
			}
		}
		return response()->json($userArray);
	}
	
	public function saveUser(Request $request)
	{
		$returnArray = array();
		$this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$request->user_id,
            'personal_code' => 'required|numeric|unique:users,personal_code,'.$request->user_id.'|digits:11',
            'phone' => 'required|numeric',
            'active' => 'required|integer',
            'dead' => 'required|integer',
            'lang' => 'required|string',
        ]);
		
		if($request->user_id){
			$user	=	User::find($request->user_id);
			if(!empty($user)){
				$user->first_name		=	$request->first_name;
				$user->last_name		=	$request->last_name;
				$user->email			=	$request->email;
				$user->personal_code	=	$request->personal_code;
				$user->phone			=	$request->phone;
				$user->active			=	$request->active;
				$user->dead				=	$request->dead;
				$user->lang				=	$request->lang;
				$user->save();
			}
			$returnArray['message']		=	"User details has been saved";
		}else{
			$user_id = User::max('user_id');
			User::create([
				'user_id' 		=> $user_id+1,
				'first_name' 	=> ucfirst($request->first_name),
				'last_name' 	=> ucfirst($request->last_name),
				'email' 		=> $request->email,
				'personal_code' => $request->personal_code,
				'phone' 		=> $request->phone,
				'active' 		=> $request->active,
				'dead' 			=> $request->dead,
				'lang' 			=> $request->lang,
				'password' 		=> bcrypt($request->personal_code)
			  ]);
			  $returnArray['message']	=	"New user has been created";
		}
        return response()->json($returnArray, 200);
	}
	
	public function deleteUser(Request $request)
	{
		$user = User::find($request->user_id);
		$user->dead = 1;
		$user->save();
		$returnArray['message']	=	"User has been deleted successfully";
		return response()->json($returnArray);
	}
}
