<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use File;

class insert_user_data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert_user_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To insert data in user table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = File::get("database/data/users.json");
        $data = json_decode($json);
		if(!empty($data)){
			foreach($data as $obj) {
			  $user_array = array(
				'user_id' 		=> $obj->id,
				'first_name' 	=> $obj->first_name,
				'last_name' 	=> $obj->last_name,
				'email' 		=> $obj->email,
				'personal_code' => $obj->personal_code,
				'phone' 		=> $obj->phone,
				'active' 		=> $obj->active,
				'dead' 			=> $obj->dead,
				'lang' 			=> $obj->lang,
				'password' 		=> bcrypt($obj->personal_code),
			  );
			  $user = User::updateOrCreate(
					[
						'email' => $obj->email
					],
					$user_array
				);
			}
			$this->info('user details are inserted/updated');
		}else{
			$this->error('No user data found');
		}
    }
}
