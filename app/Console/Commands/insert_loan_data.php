<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Loan;
use File;

class insert_loan_data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert_loan_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To insert data in loan table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = File::get("database/data/loans.json");
        $data = json_decode($json);
		if(!empty($data)){
			foreach($data as $obj) {
			  $loan_array = array(
				'user_id' 		=> $obj->user_id,
				'amount' 		=> $obj->amount,
				'interest' 		=> $obj->interest,
				'duration' 		=> $obj->duration,
				'start_date' 	=> date('Y-m-d',$obj->start_date),
				'end_date' 		=> date('Y-m-d',$obj->end_date),
				'campaign' 		=> $obj->campaign,
				'status' 		=> $obj->status
			  );
			  $user = Loan::create($loan_array);
			}
			$this->info('loan details are inserted');
		}else{
			$this->error('No loan data found');
		}
    }
}
