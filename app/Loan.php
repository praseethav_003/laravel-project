<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
		'amount',
		'interest',
		'duration',
		'start_date',
		'end_date',
		'campaign',
		'status'
    ];
	
	/**
	 * Get the user that loan belongs to.
	*/
	public function user()
	{
		return $this->hasOne('App\User','user_id','user_id');
	}
}
