<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/users-list', 'UserController@getUsersList')->name('users.list');
Route::post('/edit-user', 'UserController@saveUser')->name('users.save');
Route::post('/delete-user', 'UserController@deleteUser')->name('users.delete');
Route::get('/loans', 'LoanController@index')->name('loans');
Route::get('/loans-list', 'LoanController@getLoanList')->name('loans.list');
Route::post('/edit-loan', 'LoanController@saveLoan')->name('loans.save');
Route::post('/delete-loan', 'LoanController@deleteLoan')->name('loans.delete');
