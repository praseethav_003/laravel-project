<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@admin.com',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'personal_code' => '0',
            'phone' => '0',
            'role' => 'admin',
            'password' => bcrypt('admin@123')
        ]);
    }
}
