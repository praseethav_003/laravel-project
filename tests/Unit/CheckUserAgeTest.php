<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Helper;

class CheckUserAgeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserAge()
    {
		$perosonal_code = '49101030225';
		$age			= Helper::getUserAge($perosonal_code);
		if($age > 25)
			$this->assertTrue(true);
		else
			$this->fail('You are not able to apply for a loan');
    }
}
