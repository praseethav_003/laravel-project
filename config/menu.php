<?php

return [

    //MENU
	'menu' => [
        [
            'title' => 'My actions',
            'link' => '/home',
            'active' => 'home*'
        ],
        [
            'title' => 'Loans',
            'link' => '/loans',
            'active' => 'loans*'
        ],
        [
            'title' => 'Users',
            'link' => '/users',
            'active' => 'users*'
        ]
    ]
];
