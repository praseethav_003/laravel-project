## Install

- **clone project**
- **cd into project**
- **composer install**
- **create .env with db credentials (Used MySQL)**
- **php artisan key:generate**
- **php artisan migrate --seed**
- **npm install**
- **npm run dev**
- **php artisan serve**

## Console Commands

1. php artisan insert_user_data
2. php artisan insert_loan_data

As user_id field is provided in the json files, I have stored that too for relating the loan to each user.


## Admin Credentials

Email : admin@admin.com
password : admin@123

## Notes

**User**

When deleting user I have changed the field dead to  1.

**Loan**

When deleting loan I have changed the field status to 3.

When creating loan, not checked condition for unde age. (This can be done)

## Tests

php vendor/phpunit/phpunit/phpunit --filter CheckUserAgeTest

Change perosnal code value in \tests\Unit\CheckUserAgeTest.php